<?php

namespace App\Controller;

use App\Service\FlickrApiService;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\Length;
use App\Service\ImageCollectionService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SearchController extends Controller
{
    /**
     * @Route("/search", name="search")
     */
    public function index(Request $request, FlickrApiService $flickrApiService, ImageCollectionService $imageCollectionService)
    {

        $defaultData = array('message' => 'Search...');

        $params = $this->getParameter('search_keywords');

        $searchForm = $this->createFormBuilder($defaultData)
            ->add('keywords', ChoiceType::class, array(
                    'constraints' => new Length(array('min' => 3)),
                    'choices' => $params,
                    'choice_label' => function ($choiceValue, $key, $value) {
                        return $value;
                    }
                )
            )
            ->add('Search', SubmitType::class)
            ->getForm();

        $searchForm->handleRequest($request);

        $collection = null;

        if ($searchForm->isSubmitted() && $searchForm->isValid()) {
            $data = $searchForm->getData();

            $response = $flickrApiService->searchByString($data['keywords']);
            $collection = $imageCollectionService->getQueueFromResponse($response);

        }

        return $this->render('search/index.html.twig', [
            'searchForm' => $searchForm->createView(),
            'images' => $collection,
        ]);
    }
}
