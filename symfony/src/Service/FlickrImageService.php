<?php

namespace App\Service;

use App\Classes\Image;

class FlickrImageService
{
    public function getImage($farmId, $serverId, $id, $sercret, $title)
    {
        return new Image($this->getImageUrl($farmId, $serverId, $id, $sercret), $title);
    }

    private function getImageUrl($farmId, $serverId, $id, $sercret)
    {
        return 'http://farm' . $farmId . '.staticflickr.com/' . $serverId . '/' . $id . '_' . $sercret . '.jpg';
    }
}