<?php

namespace App\Service;

use Symfony\Component\Validator\Constraints\Collection;

class ImageCollectionService
{
    private $flickrImageService;
    private $collection = array();

    public function __construct(FlickrImageService $flickrImageService)
    {
        $this->flickrImageService = $flickrImageService;
    }

    public function getQueueFromResponse($response)
    {
        foreach ($response->photos->photo as $photo) {
            $image = $this->flickrImageService->getImage(
                $photo->farm,
                $photo->server,
                $photo->id,
                $photo->secret,
                $photo->title
            );
            $this->collection[] = $image;
        }
        return $this->getCollection();
    }

    public function getCollection()
    {
        return $this->collection;
    }

}