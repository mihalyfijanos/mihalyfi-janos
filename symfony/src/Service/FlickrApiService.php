<?php

namespace App\Service;

class FlickrApiService
{
    private $url = 'https://api.flickr.com/services/rest/?method=flickr.photos.search';
    private $parameters = array(
        'api_key' => '57f694132e4714c29a64c9af890b124e',
        'per_page' => '30',
        'format' => 'json',
        'nojsoncallback' => '1',
    );
    public function __construct()
    {
        foreach ($this->parameters as $key => $value) {
            $this->url .= '&' . $key . '=' . $value;
        }
    }

    public function searchByString($searchString)
    {
        $searchString = str_replace(' ', '', $searchString);
        $keywords = explode(',', $searchString);
        return $this->search($keywords);
    }

    public function search(array $keywords)
    {
        return $this->getResponseForSearch($this->getSearchUrl($keywords));
    }

    private function getSearchUrl(array $keywords)
    {
        return $this->url . '&tags=' . implode(',', $keywords);
    }

    private function getResponseForSearch(string $searchUrl)
    {
        return json_decode(file_get_contents($searchUrl));
    }
}
