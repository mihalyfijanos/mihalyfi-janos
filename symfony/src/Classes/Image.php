<?php

namespace App\Classes;

class Image
{
    private $imageUrl;
    private $title;
    private $sizeX;
    private $sizeY;

    public function __construct($imageUrl, $title)
    {
        $this->imageUrl = $imageUrl;
        $this->title = $title;
        list($this->sizeX, $this->sizeY) = getimagesize($this->imageUrl);
    }

    public function getX()
    {
        return $this->sizeX;
    }

    public function getY()
    {
        return $this->sizeY;
    }

    public function getSrc()
    {
        return $this->imageUrl;
    }

    public function getTitle()
    {
        return $this->title;
    }

}